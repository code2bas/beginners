import os
import configparser
cfg = configparser.ConfigParser()
cfg.read('config/config.txt')

### ----------------------------
mobile_new       = cfg['wordding']['mobile_new']
ba_no            = cfg['wordding']['ba_no']
account_name_new = cfg['wordding']['account_name_new']+' ทดสอบ'

mb_flag          = cfg['position']['mb_flag']
mb_start         = int(cfg['position']['mb_start'])
mb_end           = int(cfg['position']['mb_end'])

ba_flag          = cfg['position']['ba_flag']
ba_start         = int(cfg['position']['ba_start'])
ba_end           = int(cfg['position']['ba_end'])

acc_flag         = cfg['position']['acc_flag']
acc_start        = int(cfg['position']['acc_start'])
acc_end          = int(cfg['position']['acc_end'])

source_path       = cfg['path']['source_path']
terminal_path     = cfg['path']['terminal_path']
### ----------------------------
if mb_flag == 'Y' :
    print ('Replace Mobile : ' + mobile_new) 
if ba_flag == 'Y' :
    print ('Replace Ba No : ' + ba_no)     
if acc_flag == 'Y' :
     print ('Replace Account : ' + account_name_new) 

print ('Source Folder : ' + source_path)
print ('Terminal Folder : ' + terminal_path)
print('----------------------------------------------------------------')     
try: 
    lst_file = os.listdir(source_path)
    for file in lst_file:
        s_file_full_path=source_path+'/'+ file
        t_file_full_path=terminal_path +'/'+ file
        f_import = open(s_file_full_path, 'r') 
        f_export = open(t_file_full_path, 'w')
        Lines = f_import.readlines()
        line_cnt = 0
        for line in Lines: 
            lineM = line
            line_cnt += 1
            if line[0:1] == 'D' :            
                #Replace Mobile
                if mb_flag == 'Y' :
                    str_data = lineM[mb_start-1:mb_end-1]
                    #print("str_data :" + str_data)
                    if len(str_data.strip()) >0 :
                        lineM = lineM[0:mb_start-1] + (mobile_new + '                                                  ')[0:mb_end-mb_start] + lineM[mb_end-1:]
                        #print('*' +mobile_new + '                                                  '[0:mb_end-mb_start]+'*' )
                #Replace Acciount No
                if ba_flag == 'Y' :
                    str_data = lineM[ba_start-1:ba_end-1]
                    #print("str_data :" + str_data)
                    if len(str_data.strip()) >0 :
                        lineM = lineM[0:ba_start-1] + (ba_no + '                                                  ')[0:ba_end-ba_start] + lineM[ba_end-1:]
                        #print('*'  +ba_no + '                                                  '[0:ba_end-ba_start] +'*' )
                 #Replace Acciount Name
                if acc_flag == 'Y' :
                    str_data = lineM[acc_start-1:acc_end-1]
                    #print("str_data :" + str_data)
                    if len(str_data.strip()) >0 :
                        lineM = lineM[0:acc_start-1] + (account_name_new + '                                                  ')[0:acc_end-acc_start] + lineM[acc_end-1:]
                        #print(lineM[0:acc_start-1] + account_name_new + '                                                  '[0:acc_end-acc_start] + lineM[acc_end-1:])
            f_export.writelines(lineM)
        print('file Close   : ' + file)
        f_export.close()
        print('file Suscess : ' + file + ' , Total Row : {} '.format(line_cnt))
        print('----------------------------------------------------------------')
except ValueError:
    print("Error ")
    f_export.close()