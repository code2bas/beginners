# Copyright 2018 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# [START gmail_quickstart]
from __future__ import print_function
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials 
import base64
from bs4 import BeautifulSoup
from pprint import pprint

# If modifying these scopes, delete the file token.json.
SCOPES = ['https://www.googleapis.com/auth/gmail.readonly']

def ListMessagesWithLabels(service, user_id, label_ids=[]):
  """List all Messages of the user's mailbox with label_ids applied.
  Args:
    service: Authorized Gmail API service instance.
    user_id: User's email address. The special value "me"
    can be used to indicate the authenticated user.
    label_ids: Only return Messages with these labelIds applied.
  Returns:
    List of Messages that have all required Labels applied. Note that the
    returned list contains Message IDs, you must use get with the
    appropriate id to get the details of a Message.
  """
  try:
    response = service.users().messages().list(userId=user_id,labelIds=label_ids,maxResults=500).execute();
    #response = service.users().messages().list('me').setQ('after:1504483200 before:1504915199').execute();
    messages = []
    if 'messages' in response:
      messages.extend(response['messages'])

    while 'nextPageToken' in response:
      page_token = response['nextPageToken']

      response = service.users().messages().list(userId=user_id,labelIds=label_ids,pageToken=page_token,maxResults=500).execute()

      messages.extend(response['messages'])

      print('... total %d emails on next page [page token: %s], %d listed so far' % (len(response['messages']), page_token, len(messages)))
      sys.stdout.flush()

    return messages

  except errors.HttpError as error:
    print('An error occurred: %s' % error)

def ReadEmailDetails(service, user_id, msg_id):
  temp_dict = { }
  try:
    label_id = 'UNREAD' # ID of user label to add
    filter = {
        'criteria': {
            'from': 'TradingView <noreply@tradingview.com'
        } 
    }
    #message = service.users().settings().filters().create(userId='me', body=filter).execute()
    message = service.users().messages().get(userId=user_id, id=msg_id).execute() # fetch the message using API
    #result = gmail_service.users().settings().filters().create(userId='me', body=filter).execute()
     

    #print(message)
    payld = message['payload'] # get payload of the message
    snippet = message['snippet'] # get payload of the message
    headr = payld['headers'] # get header of the payload
    bodys = payld['body'] # get header of the payload
      
    #sublect = message['payload']['headers']['Subject'] # get payload of the message
    for one in headr: # getting the Subject
        if one['name'] == 'From':
          msg_From = one['value']
           
        if one['name'] == 'Subject':
          msg_Subject = one['value']
           
        if one['name'] == 'Date':
          msg_Date = one['value'] 
                              
      
    #part_data = bodys['data']
    #clean_one = part_data.replace("-","+") # decoding from Base64 to UTF-8
    #clean_one = clean_one.replace("_","/") # decoding from Base64 to UTF-8
    #clean_two = base64.b64decode (bytes(clean_one, 'UTF-8')) # decoding from Base64 to UTF-8 
    #soup = BeautifulSoup(clean_two , "lxml" ) 
    #msg_Body = soup.body() 
       
      
    if msg_From == 'TradingView <noreply@tradingview.com>' : 
        temp_dict['From'] = msg_From
        temp_dict['snippet'] = snippet
        temp_dict['Subject'] = msg_Subject
        temp_dict['Date'] = msg_Date
        #temp_dict['Body'] = msg_Body
    else :
        temp_dict = None
        
      #if 'BTCUSDT' in snippets :
      #  print(sublect) 
        
      #for one in headr: # getting the Subject
      #    if one['name'] == 'Subject':
      #        msg_subject = one['value']
      #        temp_dict['Subject'] = msg_subject
      #    else:
      #        pass


      #for two in headr: # getting the date
      #    if two['name'] == 'Date':
      #         msg_date = two['value']
      #        # date_parse = (parser.parse(msg_date))
      #        # m_date = (date_parse.datetime())
      #        temp_dict['DateTime'] = msg_date
      #    else:
      #        pass


      ## Fetching message body
      #email_parts = payld['parts'] # fetching the message parts
      #part_one  = email_parts[0] # fetching first element of the part
      #part_body = part_one['body'] # fetching body of the message 
      #part_data = part_body['data'] # fetching data from the body 
      #clean_one = part_data.replace("-","+") # decoding from Base64 to UTF-8
      #clean_one = clean_one.replace("_","/") # decoding from Base64 to UTF-8
      #clean_two = base64.b64decode (bytes(clean_one, 'UTF-8')) # decoding from Base64 to UTF-8 
      #soup = BeautifulSoup(clean_two , "lxml" ) 
      #message_body = soup.body() 
      # message_body is a readible form of message body
      # depending on the end user's requirements, it can be further cleaned
      # using regex, beautiful soup, or any other method
      #temp_dict['Message_body'] = message_body

  except Exception as e:
      print(e)
      temp_dict = None
      pass

  finally:
      return temp_dict
      
def main():
    """Shows basic usage of the Gmail API.
    Lists the user's Gmail labels.
    """
    creds = None
    # The file token.json stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.json'):
        creds = Credentials.from_authorized_user_file('token.json', SCOPES)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.json', 'w') as token:
            token.write(creds.to_json())

    service = build('gmail', 'v1', credentials=creds)

    # Call the Gmail API
    results = service.users().labels().list(userId='me').execute()
    #print(results)
    labels = results.get('labels', [])

    #if not labels:
    #    print('No labels found.')
    #else:
    #    print('Labels:')
    #    for label in labels:
    #        print(label['name'])
    user_id =  'me'       
    email_list = ListMessagesWithLabels(service, user_id, [])
    #print(email_list) 
    email_dict = []
    for email in email_list:
        msg_id = email['id'] # get id of individual message 
        data = ReadEmailDetails(service,user_id,msg_id)
        if data is not None:
            email_dict.append(data) 

    pprint(email_dict) 

if __name__ == '__main__':
    main()
# [END gmail_quickstart]